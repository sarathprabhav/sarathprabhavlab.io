---
title: "Outreach Activities"
date: 2022 - -09-07T18:43:05+05:30
draft: false
---

Aug 27-28, 2022 -  **Sky Observation and Camping** , Sayalgudi Beach Side
Camp , organised by StarVoirs & Discover Alpas.

Jul 9, 2022 -  **Know the Moon**, Kerala Science and Technology Museum and
Planetarium, organised by AASTRO Kerala.

May 23, 2022 -  **Budget Astrophotography and Night Sky Watching**, Kerala
Arts & Craft Village, Kovalam organised by UL Space Club, Calicut.

May 8, 2022 -  **Mobile Astrophotography Workshop**, SMV Library, Peringammala.

Mar 27, 2022 -  **Hands-on Night Photography Workshop**, Mar Ivanios
College, Thiruvananthapuram.

Mar 2, 2022 -  **How Big is Our Universe**, Govt. UP School, Anachal,
Thiruvananthapuram.

Dec 4, 2021 -  **ABC of Stargazing**, LBSIT, Thiruvananthapuram as part of
EXPLORICA, IEEE Event.

Nob 28, 2021 -  **Budget Astrophotography**, online talk, organised by
Sasthrasnehi

Sep 10, 2021 -  **Sky Coordinates & Basic Astronomy**, as part of online
course Data Driven Astronomy organised by Positron Foundation.

Jun 6, 2021 -  **Stellar Evolution**, RCET, Kochi organised by Positron
Foundation.

Jan, 2021 -  **A Course on Electrostatics**, for Swayamprabha DTH Channel
recorded at EMRC, University of Calicut.

Dec 21, 2020 -  **Observation & Live Streaming of Saturn -- Jupiter Great
Conjunction**, CUSAT, Kochi, Organised by IUCKLAM, IUCAA Resource Centre
and Department of Physics, CUSAT.

Jun 4, 2020 -  **Sky Coordinates & Basic Astronomy**, as part of online
course Data Driven Astronomy organised by Positron Foundation.

Apr 26, 2020 -  **Basics of Astronomy - One Month online course**,
organised by Positron Foundation for High School Students.

Feb 21, 2020 -  **Astrophotography, Postprocessing & Skywatching**, as part
of two-day workshop at St. Theresa's College, Kochi, organised by
Positron Foundation.

Dec 12, 2019 -  **Talk on Solar Eclipse and Day Time Astronomy**, Govt.
H.S.S., Nadakkavu, Kozhikode, organised by Positron Foundation.

Oct 17, 2019 -  **Full day workshop on Astronomy**, at E.M.E.A. H.S.S.
Kondotty, organized by Positron Foundation.

Nov 23, 2019 -  **Talk on Solar Eclipse and Demonstration**, at Govt.
College, Karyavattom, organized by IUCKLAM CUSAT.

Sep 2, 2019 -  **A day long Astronomy workshop & Sky Observation**, at
Malayalam Mission, IGCAR, Kalpakkam organised by Positron Foundation.

Aug 24-25, 2019 -  **Basic Sky Movements Coordinates & Basics of
Photography**, at S.S.N. Colleges, Chennai, organised by Positron
Foundation.

Mar 30-31, 2019 -  **Basics astronomy & Basics of Photography**, at
Maharajas College, Kochi, organised by Positron Foundation.

